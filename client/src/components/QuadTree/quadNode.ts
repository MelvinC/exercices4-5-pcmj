import { ColliderComponent } from "../colliderComponent";
import { Rectangle } from "../rectangle";

export class QuadNode{
    private childrens:QuadNode[]=[];
    private static MAX_DEPTH:number = 3;
    private components:ColliderComponent[]=[];
    private depth!:number;
    private rect!:Rectangle;

    constructor(_rect:Rectangle, _depth:number, _components:ColliderComponent[]){
        this.rect=_rect;
        this.depth=_depth;
        for(var i = 0; i < _components.length; i++){
            if (this.rect.intersectsWith(_components[i].area))
            this.components.push(_components[i]);
        }
        if(this.depth < QuadNode.MAX_DEPTH){
            this.childrens.push(new QuadNode(new Rectangle({xMin:this.rect.xMin, xMax:(this.rect.xMin + (this.rect.xMax-this.rect.xMin)/2), yMin:this.rect.yMin, yMax:(this.rect.yMin + (this.rect.yMax-this.rect.yMin)/2)}), this.depth+1, this.components));
            this.childrens.push(new QuadNode(new Rectangle({xMax:this.rect.xMax, xMin:(this.rect.xMin + (this.rect.xMax-this.rect.xMin)/2), yMin:this.rect.yMin, yMax:(this.rect.yMin + (this.rect.yMax-this.rect.yMin)/2)}), this.depth+1, this.components));
            this.childrens.push(new QuadNode(new Rectangle({xMin:this.rect.xMin, xMax:(this.rect.xMin + (this.rect.xMax-this.rect.xMin)/2), yMax:this.rect.yMax, yMin:(this.rect.yMin + (this.rect.yMax-this.rect.yMin)/2)}), this.depth+1, this.components));
            this.childrens.push(new QuadNode(new Rectangle({xMax:this.rect.xMax, xMin:(this.rect.xMin + (this.rect.xMax-this.rect.xMin)/2), yMax:this.rect.yMax, yMin:(this.rect.yMin + (this.rect.yMax-this.rect.yMin)/2)}), this.depth+1, this.components));
        }
    }

    public getComponents():ColliderComponent[]{
        return this.components;
    }
    
    public getChildren():QuadNode[]{
        return this.childrens;
    }
    

}