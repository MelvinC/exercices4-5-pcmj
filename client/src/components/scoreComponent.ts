import { NetworkScore } from "../../../common/messages";
import { EventTrigger } from "../eventTrigger";
import { GlobalConfig } from "../main";
import { Component } from "./component";
import { NetworkingComponent } from "./networkingComponent";
import { PlayerComponent } from "./playerComponent";
import { TextSpriteComponent } from "./textSpriteComponent";

// # Classe *ScoreComponent*
interface IScoreComponentDesc {
  scoreSprite: string;
  networking: string;
  player:string;
}

export class ScoreComponent extends Component<IScoreComponentDesc> {
  private scoreChangedEvent = new EventTrigger();
  private scoreSprite!: TextSpriteComponent;
  private _value!: number;
  private networking!: NetworkingComponent;
  private player!: PlayerComponent;

  // ## Méthode *setup*
  // Cette méthode conserve le composant de texte qui affiche
  // le pointage, et initialise sa valeur.
  public setup(descr: IScoreComponentDesc) {
    this.networking = Component.findComponent<NetworkingComponent>(descr.networking)!;
    this.scoreSprite = Component.findComponent<TextSpriteComponent>(descr.scoreSprite)!;
    this.player = Component.findComponent<PlayerComponent>(descr.player)!;
    this.value = 0;
  }

  // ## Propriété *value*
  // Cette méthode met à jour le pointage et l'affichage de
  // ce dernier.
  get value() {
    return this._value;
  }

  set value(newVal) {
    this._value = newVal;
    this.scoreChangedEvent.trigger(this.value);
    if(this.player.isLocal){
      this.sendValue();
      this.scoreSprite.text = this.value.toString();
    }
  }

  private sendValue(){
    var msg = new NetworkScore();
    msg.build({score:this.value, player:GlobalConfig.alias});
    console.log(msg);
    this.networking.send(msg);
  }
}
